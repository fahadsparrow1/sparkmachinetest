//
//  LoginModel.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import Foundation

struct LoginModel : Codable {
    let refresh : String?
    let access : String?
    let username : String?
    let firstname : String?
    let lastname : String?
    let email : String?

    enum CodingKeys: String, CodingKey {

        case refresh = "refresh"
        case access = "access"
        case username = "username"
        case firstname = "firstname"
        case lastname = "lastname"
        case email = "email"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        refresh = try values.decodeIfPresent(String.self, forKey: .refresh)
        access = try values.decodeIfPresent(String.self, forKey: .access)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
        lastname = try values.decodeIfPresent(String.self, forKey: .lastname)
        email = try values.decodeIfPresent(String.self, forKey: .email)
    }

}

