//
//  dashboardModel.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 22/02/24.
//

import Foundation

struct DashboardModel : Codable {
    let status : String?
    let images : [Images]?
    var visit : [Visit]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case images = "images"
        case visit = "visit"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        images = try values.decodeIfPresent([Images].self, forKey: .images)
        visit = try values.decodeIfPresent([Visit].self, forKey: .visit)
    }

}

struct Images : Codable {
    let id : Int?
    let image_link : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case image_link = "image_link"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        image_link = try values.decodeIfPresent(String.self, forKey: .image_link)
    }

}

struct Visit : Codable {
    let id : Int?
    let user : User?
    let place : String?
    let visited_date : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user = "user"
        case place = "place"
        case visited_date = "visited_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user = try values.decodeIfPresent(User.self, forKey: .user)
        place = try values.decodeIfPresent(String.self, forKey: .place)
        visited_date = try values.decodeIfPresent(String.self, forKey: .visited_date)
    }

}

struct User : Codable {
    let id : Int?
    let first_name : String?
    let last_name : String?
    let username : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case first_name = "first_name"
        case last_name = "last_name"
        case username = "username"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        username = try values.decodeIfPresent(String.self, forKey: .username)
    }

}

