//
//  RestServices.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import Foundation
import Alamofire

class ApiServices {
    
    static func getAccessToken() -> String{
        let accessToken = UserDefaults.standard.value(forKey: accessToken)as? String
        if accessToken == "" || accessToken == nil
        {
            return ""
        }
        else
        {
            let token = "Bearer " + accessToken!
            return token
        }
        
    }
    
    static let baseUrl = "http://148.251.86.36:8001/"

    static func userLogin(params: Data, complete: @escaping (LoginModel?, Int?, Error?) -> Void) {
        let url = baseUrl + "login/"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = params
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                if let httpResponse = response as? HTTPURLResponse {
                    let statusCode = httpResponse.statusCode
                    if statusCode == 200 {
                        if let data = data {
                            do {
                                let jsonDecoder = JSONDecoder()
                                let responseOBJ = try jsonDecoder.decode(LoginModel.self, from: data)
                                complete(responseOBJ, statusCode, nil)
                            } catch {
                                complete(nil, statusCode, error)
                            }
                        } else if let error = error {
                            print("error 123")
                            complete(nil, statusCode, error)
                        } else {
                            print("error 133")
                            complete(nil, nil, LoginError.invalidCredentials)
                        }
                    } else {
                    
                        print("error 153")
                        complete(nil, statusCode, nil)
                    }
                } else {
                   print("error 143")
                    complete(nil, nil, LoginError.invalidCredentials)
                }
            }
        }
        
        task.resume()
    }

    
    static func getDashboardData(complete: @escaping (DashboardModel?, Error?) -> Void) {
        let URL = baseUrl+"dashboard/"
        var request = URLRequest(url: NSURL.init(string: URL)! as URL)
        request.httpMethod = "GET"
        request.addValue(self.getAccessToken(), forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                complete(nil, error)
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let responseOBJ = try jsonDecoder.decode(DashboardModel.self, from: data)
                complete(responseOBJ, nil)
            } catch {
                complete(nil, error)
            }
        }
        
        task.resume()
    }
    
    
    
    
}
