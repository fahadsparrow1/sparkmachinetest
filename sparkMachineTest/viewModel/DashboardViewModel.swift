//
//  DashboardViewModel.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import Foundation
import UIKit

class DashboardViewModel {
    var data: DashboardModel?
   
    
    func getImgURL(for id: Int) -> String? {
           guard let images = data?.images else {
               return nil
           }
           
           if let image = images.first(where: { $0.id == id }) {
               return image.image_link
           } else {
               return nil
           }
       }
    
    var onDataLoad: (() -> Void)?
    
    
    func getDashboardData() {
        ApiServices.getDashboardData { data, error in
            if let responseData = data {
                self.data = responseData
                self.onDataLoad?()
            } else {
               
            }
        }
    }

    
    func logout() {
        UserDefaults.standard.removeObject(forKey: accessToken)
    }
}
