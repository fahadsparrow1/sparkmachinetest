//
//  LoginViewModel.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import Foundation

class LoginViewModel {
    
    var username: String = ""
    var password: String = ""
    var loginSuccess: (() -> Void)?
    var loginFailure: ((Error) -> Void)?
    weak var delegate: LoginDelegate?

    
    func login() {
        guard !username.isEmpty, !password.isEmpty else {
            loginFailure?(LoginError.emptyCredentials)
            return
        }
        
        let parameters: [String: String] = ["username": username, "password": password]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            
            ApiServices.userLogin(params: postData) { data, status, error  in
                if let responseData = data {
                    UserDefaults.standard.setValue(responseData.access, forKey: accessToken)
                    self.loginSuccess?()
                } else {
                    self.loginFailure?(LoginError.invalidCredentials)
                }
            }
        } catch {
            
            loginFailure?(error)
        }
    }

}



enum LoginError: Error {
    case emptyCredentials
    case invalidCredentials
}
