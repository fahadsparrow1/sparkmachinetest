//
//  DashboardCell.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import UIKit

class DashboardCell: UITableViewCell {

  
    @IBOutlet var imgViv: UIImageView!
    
    @IBOutlet var lbl: UILabel!
    
    
    @IBOutlet var prviewBtn: UIButton!
    
}
