//
//  File.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 23/02/24.
//

import UIKit
import Kingfisher

class CustomAlertView: UIView {
   
    var cancelActionHandler: ((Int) -> Void)?

    init(bound: CGRect, title: String, url: String, index: Int) {
        super.init(frame: bound)
        
        let overlayView = UIView(frame: CGRect(x: 15, y: bound.height/2-125, width: bound.width-30, height: 250) )
        overlayView.backgroundColor = UIColor.white
        overlayView.layer.cornerRadius = 16.0
        
        addSubview(overlayView)
        
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.black
       
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        let url = URL(string: url)
        imageView.kf.setImage(with: url)
        
        
        let deleteButton = UIButton(type: .system)
        deleteButton.layer.cornerRadius = 16.0
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.setTitleColor(UIColor.white, for: .normal)
        deleteButton.backgroundColor = UIColor.green
        deleteButton.tag = index
        deleteButton.addTarget(self, action: #selector(cancelButtonTapped(_:)), for: .touchUpInside)
        
        
        let okButton = UIButton(type: .system)
        okButton.layer.cornerRadius = 16.0
        okButton.setTitle("OK", for: .normal)
        okButton.setTitleColor(UIColor.white, for: .normal)
        okButton.backgroundColor = UIColor.orange
        okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
        
        
        overlayView.addSubview(imageView)
        overlayView.addSubview(titleLabel)
        overlayView.addSubview(deleteButton)
        overlayView.addSubview(okButton)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints   = false
        imageView.translatesAutoresizingMaskIntoConstraints    = false
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        okButton.translatesAutoresizingMaskIntoConstraints     = false
        
        NSLayoutConstraint.activate([
        
            titleLabel.topAnchor.constraint(equalTo: overlayView.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: overlayView.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: overlayView.trailingAnchor, constant: -20),
            
            imageView.topAnchor.constraint(equalTo: overlayView.topAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: overlayView.leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: overlayView.trailingAnchor, constant: -0),
            imageView.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor, constant: -0),
            
            
            deleteButton.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor, constant: -20),
            deleteButton.leadingAnchor.constraint(equalTo: overlayView.leadingAnchor, constant: 20),
            deleteButton.widthAnchor.constraint(equalTo: overlayView.widthAnchor, multiplier: 0.4),
            deleteButton.heightAnchor.constraint(equalToConstant: 40),
            
            okButton.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor, constant: -20),
            okButton.trailingAnchor.constraint(equalTo: overlayView.trailingAnchor, constant: -20),
            okButton.widthAnchor.constraint(equalTo: overlayView.widthAnchor, multiplier: 0.4),
            okButton.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        
    }

    @objc func okButtonTapped() {
        removeFromSuperview()
    }

    @objc func cancelButtonTapped(_ sender: UIButton) {
        cancelActionHandler!(sender.tag)
        removeFromSuperview()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
