//
//  ViewController.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 22/02/24.
//

import UIKit
import Kingfisher

class ViewController: UIViewController{

    @IBOutlet var mainTableview: UITableView!
    let viewModel = DashboardViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Spark Dash board"
        view.backgroundColor = .orange
        mainTableview.delegate = self
        mainTableview.dataSource = self
        
        
        let defaults = UserDefaults.standard
        if defaults.value(forKey: accessToken) == nil {
            presentLoginScreen()
        } else {
            
            setupUI()
            
        }
        
    }

    func presentLoginScreen() {
        let loginViewController = LoginViewController()
        let loginNavigationController = UINavigationController(
            rootViewController: loginViewController)
        loginNavigationController.modalPresentationStyle = .fullScreen
        loginViewController.delegate = self
        present(loginNavigationController, animated: false, completion: nil)
    }
    
    func setupUI() {
        
        viewModel.getDashboardData()
        viewModel.onDataLoad = { [weak self] in
            DispatchQueue.main.async {
                        self?.mainTableview.reloadData()
                    }
           // print(self?.viewModel.data)
        }
        
        
    }
    
    @IBAction func logoutAction(_ sender: Any) {
    
        viewModel.logout()
        presentLoginScreen()
       
        
    }
    

}

extension ViewController : UITableViewDelegate, UITableViewDataSource, LoginDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = viewModel.data?.visit
        return data?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? DashboardCell,
              let data = viewModel.data?.visit?[indexPath.row]
        
        else {return UITableViewCell()}
       
        let string = "\(data.user!.first_name!) \(data.user!.last_name!)"
        let urlString = viewModel.getImgURL(for: data.id!)
        let url = URL(string: urlString!)
        cell.imgViv.contentMode = .scaleAspectFill
        cell.imgViv.kf.setImage(with: url)
        cell.lbl.text = string
        cell.prviewBtn.tag = indexPath.row
        cell.prviewBtn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("touched")
//        let data = viewModel.data?.visit![indexPath.row]
//        let string = "\(data!.user!.first_name!) \(data!.user!.last_name!)"
//        let urlString = viewModel.getImgURL(for: data!.id!)
//        let alert = CustomAlertView(bound: view.bounds, title: string, url: urlString!, index: indexPath.row)
//        alert.cancelActionHandler = { [weak self] index in
//            
//            guard let viewModel = self?.viewModel else { return }
//                   
//                   viewModel.data?.visit?.remove(at: index)
//                   tableView.reloadData()
//           
//        }
//        view.addSubview(alert)
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func loginSuccess() {
        print("Login successful. Setting up UI.")
            setupUI()
        }
    
    @objc func buttonTapped(_ sender: UIButton) {
        print("touched")
        let data = viewModel.data?.visit![sender.tag]
        let string = "\(data!.user!.first_name!) \(data!.user!.last_name!)"
        let urlString = viewModel.getImgURL(for: data!.id!)
        let alert = CustomAlertView(bound: view.bounds, title: string, url: urlString!, index: sender.tag)
        alert.cancelActionHandler = { [weak self] index in
            
            guard let viewModel = self?.viewModel else { return }
            viewModel.data?.visit?.remove(at: index)
            DispatchQueue.main.async {
                        self?.mainTableview.reloadData()
                    }
            
        }
        view.addSubview(alert)
    }
    
    
}

