//
//  LoginViewController.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 22/02/24.
//

import UIKit

protocol LoginDelegate: AnyObject {
    func loginSuccess()
}


class LoginViewController: UIViewController {
    
    let usernameTextField : UITextField = {
        let text = UITextField()
        text.placeholder = "Username"
        text.borderStyle = .roundedRect
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()

    let passwordTextField : UITextField = {
        let text = UITextField()
        text.placeholder = "password"
        text.borderStyle = .roundedRect
        text.isSecureTextEntry = true
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    let loginButton : UIButton = {
        let btn = UIButton()
        btn.setTitle("Login", for: .normal)
        btn.backgroundColor = .blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()

    
    let VM = LoginViewModel()
    weak var delegate: LoginDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Login screen"
        view.backgroundColor = .yellow
        view.addSubview(usernameTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        
        setupUI()
        bindViewModel()
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
    }
    
    func setupUI() {
        NSLayoutConstraint.activate([
            usernameTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            usernameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            usernameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            passwordTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 20),
            passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 30),
            loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            loginButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func bindViewModel() {
        
        VM.loginSuccess = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.navigationController?.dismiss(animated: true) {
                    self.delegate?.loginSuccess()
                }
            }
        }
        
        VM.loginFailure = { [weak self] error in
            print("login failure")
            DispatchQueue.main.async {
                self?.SparrowSnackBar(Message: "\(error). Please try again")
            }
        }
    }

    
 
    @objc func loginButtonTapped() {
            guard let username = usernameTextField.text, !username.isEmpty else {
                self.SparrowSnackBar(Message: "username cannot be empty")
                return
            }

            guard let password = passwordTextField.text, !password.isEmpty else {
                self.SparrowSnackBar(Message: "password cannot be empty")
                return
            }

            VM.username = username
            VM.password = password
            VM.login()
        }
    

}
