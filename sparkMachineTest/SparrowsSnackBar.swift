//
//  SparrowsSnackBar.swift
//  sparkMachineTest
//
//  Created by Fahad Sparrow on 26/02/24.
//

import Foundation
import UIKit


class SnackbarView: UIView {

    let viewModel: SnackbarViewModel
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "system", size: 11)
        return label
    }()
    
    init(viewModel: SnackbarViewModel, frame:CGRect) {
        self.viewModel = viewModel
        super.init(frame: frame)
        
        addSubview(label)
        clipsToBounds = true
        layer.cornerRadius  = 8
        layer.masksToBounds = true
        backgroundColor = .systemGray6
        configure()
    }
    
    private func configure() {
        label.text = viewModel.text
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = bounds
    }
    
    public func showSparrowToast(on ViewController: UIViewController) {}
    
   
    
}



extension UIViewController{
    
    func SparrowSnackBar(Message: String) {
        var snackbarvm: SnackbarViewModel?
        snackbarvm = SnackbarViewModel(text: Message)
        let frame = CGRect(x: 0, y: 0, width: view.frame.size.width/1.05, height: 60)
        let snackbar = SnackbarView(viewModel: snackbarvm!, frame: frame)
        
        let width = view.frame.size.width
        snackbar.frame = CGRect(x: (width-(width/1.05))/2,
                                y: view.frame.size.height,
                                width: width/1.05,
                                height: 60)
        view.addSubview(snackbar)
        UIView.animate(withDuration: 0.3, animations: {
            snackbar.frame = CGRect(x: (width-(width/1.05))/2,
                                    y: self.view.frame.size.height-70,
                                    width: width/1.05,
                                    height: 60)
        },completion: {done in
            if done {
                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        snackbar.frame = CGRect(x: (width-(width/1.05))/2,
                                                y: self.view.frame.size.height-70,
                                                width: width/1.05,
                                                height: 60)
                    }, completion: {finished in
                        if finished {
                            snackbar.removeFromSuperview()
                        }
                    })
                })
                
                
            }
        })
        
    }
    
}


import Foundation


struct SnackbarViewModel{
    let text: String
}

